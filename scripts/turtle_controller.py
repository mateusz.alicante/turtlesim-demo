#!/usr/bin/env python3
import rospy
from geometry_msgs.msg import Twist
from turtlesim.msg import Pose
from my_robot_controller.srv import SquareSpecs
import math

drawing_in_progress = False
pos = Pose()

def pose_callback(msg: Pose):
    global pos
    pos = msg

def draw_square(n_sides):
    global drawing_in_progress

    rospy.loginfo("Drawing square")
    rate = rospy.Rate(300)

    rospy.loginfo(n_sides)
    stages = n_sides
    side_length = 1
    angle_per_stage = math.pi / 2 / stages * 4

    vel = 1

    curr_stage = 0
    rotating = False
    to_rotate = 0

    
    t0 = rospy.Time.now().to_sec()
    prev_t = t0
    while not rospy.is_shutdown():
        if not rotating and (rospy.Time.now().to_sec() - t0) >= (side_length / vel):

            # If this is the last side to draw, stop
            if curr_stage >= stages:
                break

            t0 = rospy.Time.now().to_sec()

            curr_stage += 1

            to_rotate = angle_per_stage
            rotating = True
        
        if rotating:
            if to_rotate > 0:
                p = Twist()
                p.angular.z = 1
                pub.publish(p)
                to_rotate -= (rospy.Time.now().to_sec() - prev_t)
            else:
                rotating = False
                t0 = rospy.Time.now().to_sec()

        else:
            p = Twist()
            p.linear.x = vel
            pub.publish(p)
            
        prev_t = rospy.Time.now().to_sec()
        rate.sleep()


            


    drawing_in_progress = False

def draw_square_request(data):
    global drawing_in_progress
    if not drawing_in_progress:
        drawing_in_progress = True
        rospy.loginfo(data)
        draw_square(data.n_sides)
        return True
    else:
        return False

if __name__ == "__main__":
    rospy.init_node('turtle_controller')
    rospy.loginfo("turtle_controller node started")

    pub = rospy.Publisher('/turtle1/cmd_vel', Twist, queue_size=10)
    sub = rospy.Subscriber('/turtle1/pose', Pose, pose_callback)
    s = rospy.Service('/turtle_controller/draw_square', SquareSpecs, draw_square_request)

    rospy.spin()
    

    # rate = rospy.Rate(10)
    # while not rospy.is_shutdown():
    #     p = Twist()
    #     p.linear.x = 1
    #     p.angular.z = 1
    #     pub.publish(p)
    #     rate.sleep()