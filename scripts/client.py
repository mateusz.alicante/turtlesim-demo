#!/usr/bin/env python3
import rospy
from my_robot_controller.srv import SquareSpecs

if __name__ == "__main__":
    rospy.init_node('turtle_client')
    rospy.loginfo("turtle_client node started")

    rospy.wait_for_service('/turtle_controller/draw_square')


    s = rospy.ServiceProxy('/turtle_controller/draw_square', SquareSpecs)
    res = s(6, 255, 255, 255)
    rospy.loginfo(res)

    rospy.spin()